<?php
	require("pages_number.php");

	if(!isset($_REQUEST))
	{
		return;
	}
	
	// Строка для подтверждения адреса сервера из настроек Callback API
	$conf_key = "f51f5550";

	// Ключ доступа сообщества
	$token = "c3fad5f25471d17bb075b4e50128bec790b2642a7eaace238c06becb685a6700988d6727234ce710fa5c8";

	// Секретный ключ, который мы создали сами
	$secret_key = "sdf23rfsd98m9n";

	// Версия VK API
	$version = "5.69";

	// Получаем и декодируем уведомление
	$data = json_decode(file_get_contents("php://input"));

	// Проверяем, что находится в поле "type"
	if( $data->type === "confirmation" )
	{
		echo $conf_key;
	}
	else if( $data->type === "message_new" )
	{
		// Проверка на secret_key
		if( $data->secret === $secret_key )
		{
			// Получаем id его автора
			$user_id = $data->object->user_id;

			// Проверяем, существуют ли вложенности
			if( property_exists($data->object, "attachments") ){
				// Считаю кол-во элементов во вложенности
				$numb_elems = count($data->object->attachments);

				// Мне нужен только 1 элемент
				if( $numb_elems == 1 )
				{
					// Делаю проверку на тип вложенности этого элемента
					// мне нужен только doc
					if( property_exists($data->object->attachments[0], "doc") )
					{
						// Проверяю тип документа, беру только pdf
						$ext = $data->object->attachments[0]->doc->ext;

						if( $ext == "pdf" )
						{
							// Ссылка на файл
							$url = $data->object->attachments[0]->doc->url;

							// Оригинальное имя файла
							$file_name = $data->object->attachments[0]->doc->title;

							// Хэш имя файла
							$hash_name = rand(100000, 999999) . "." .  $ext;

							// Если файл был загружен на сервер
							if( copy($url, $hash_name) )
							{	
								// Если файл был загружен, применяю к нему скрипт подсчета страниц
								$pages_numb = pages_number_pdf($hash_name);

								// Удаляю файл, чтобы не занимать много места на сервере
								unlink($hash_name);

								$message = "Ваш файл {$file_name} содержит {$pages_numb} листов";
							} else {
								$message = "Ваш файл {$file_name} не был загружен";
							}
						} else {
							$message = "Вы прислали файл НЕ PDF, пришлите PDF";
						}
					} else {
						$message = "Ваш файл не является документом";
					}
				} else {
					$message = "Вы прислали НЕСКОЛЬКО файлов, а нужно ОДИН";
				}
			} else {
				$message = "Вы НЕ прислали файл";
			}

			// С помощью messages.send и токена сообщества отправляем ответное сообщение
			$request_params = array(
				"message" => $message,
				"user_id" => $user_id,
				"access_token" => $token,
				"v" => $version
			);

			$http_params = http_build_query($request_params);

			file_get_contents("https://api.vk.com/method/messages.send?" . $http_params);

			echo "ok";
		}
	}