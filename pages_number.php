<?php
	function pages_number_pdf($file){
		// PARSE ENTIRE OUTPUT
	    // SURROUND WITH DOUBLE QUOTES IF FILE NAME HAS SPACES
	    exec("./pdfinfo {$file}", $output);

	    // ITERATE THROUGH LINES
	    $pagecount = 0;
	    foreach($output as $op){
	        // EXTRACT NUMBER
	        if(preg_match("/Pages:\s*(\d+)/i", $op, $matches) === 1){
	            $pagecount = intval($matches[1]);
	            break;
	        }
	    }
	    return $pagecount;
	}